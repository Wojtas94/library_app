package pl.sda.dto;

import lombok.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import pl.sda.model.Borrow;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "author")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter(value = AccessLevel.PACKAGE)
@ToString
public class BookDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;

    private String title;

    private String isbn;

    private String author;

    private String category;

    private LocalDate dateOfRelease;

    private Integer pages;

    private String borrower;

    private Boolean isBorrow;

    private List<Borrow> borrows;



}
