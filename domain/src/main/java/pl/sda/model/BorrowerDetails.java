package pl.sda.model;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "borrower_details")
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
@Getter
public class BorrowerDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_borrower_details")
    private Long idBorrowerDetails;

    private String address;

    private String email;

    private String phone;
}
