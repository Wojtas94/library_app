package pl.sda.model;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "book")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@ToString
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ("id_book"))
    private Long idBook;

    @Column(length = 2, name = "is_borrow")
    private Boolean isBorrow;

    private String category;

    @Column(length = 13)
    private String isbn;

    private Integer pages;

    @Column(name = "release_date")
    private LocalDate releaseDate;

    @Column(length = 350)
    private String summary;

    private String title;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
    private List<Borrow> borrows;
}
