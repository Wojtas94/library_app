import org.junit.After;
import org.junit.Test;
import pl.sda.model.*;
import pl.sda.model.repository.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Tests {

    AuthorRepository authorRepository = AuthorRepository.createAuthorRepository();
    BookRepository bookRepository = BookRepository.createBookRepository();
    BorrowerRepository borrowerRepository = BorrowerRepository.createBorrowerRepository();
    BorrowerDetailsRepository borrowerDetailsRepository = BorrowerDetailsRepository.createBorrowerDetailsRepository();
    BorrowRepository borrowRepository = BorrowRepository.createBorrowRepository();


    @Test
    public void deleteBook_shouldReturnTrue(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean b = authorRepository.create(author);
        assertTrue(b);
        Book book = Book.builder().author(author).isBorrow(false).category("Horror").isbn("A57693JHGJH")
                .releaseDate(LocalDate.of(2012, 2, 22)).title("Witcher").summary("Good book").build();
        boolean b1 = bookRepository.create(book);
        assertTrue(b1);
        boolean delete = bookRepository.delete(book.getIdBook());
        assertTrue(delete);
    }

    @Test
    public void createAuthor_shouldReturnTrue(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean b = authorRepository.create(author);
        assertTrue(b);
    }

    @Test
    public void updateAuthor_shouldReturnTrue(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean b = authorRepository.create(author);
        assertTrue(b);
        Author author1 = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Warszawa").idAuthor(author.getIdAuthor()).build();
        boolean update = authorRepository.update(author1);
        assertTrue(update);
    }

    @Test
    public void getAuthor_shouldReturnTrue(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean b = authorRepository.create(author);
        assertTrue(b);
        Author author1 = authorRepository.read(1L);
        System.out.println(author1);
    }

    @Test
    public void deleteAuthor_shouldReturnTrue(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        Author author1 = Author.builder().firstName("Maciek").lastName("Wilczynski").birthPlace("Bogdaniec").build();
        boolean b = authorRepository.create(author);
        boolean c = authorRepository.create(author1);
        assertTrue(b);
        assertTrue(c);
        boolean delete = authorRepository.delete(author.getIdAuthor());
        assertTrue(delete);
    }

    @Test
    public void findAll_shouldReturnList(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        Author author1 = Author.builder().firstName("Maciek").lastName("Wilczynski").birthPlace("Bogdaniec").build();
        boolean b = authorRepository.create(author);
        boolean c = authorRepository.create(author1);
        assertTrue(b);
        assertTrue(c);
        List list = authorRepository.findAll();
        System.out.println(list);
    }

    @Test
    public void createBook_shouldReturnTrue(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean b = authorRepository.create(author);
        assertTrue(b);
        Book book = Book.builder().author(author).isBorrow(true).category("Horror").isbn("A57693JHGJH")
                .releaseDate(LocalDate.of(2012, 2, 22)).title("Witcher").summary("Good book").build();
        boolean b1 = bookRepository.create(book);
        assertTrue(b1);
    }

    @Test
    public void updateBook_shouldReturnTrue(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean b = authorRepository.create(author);
        assertTrue(b);
        Book book = Book.builder().author(author).isBorrow(false).category("Horror").isbn("A57693JHGJH")
                .releaseDate(LocalDate.of(2012, 2, 22)).title("Witcher").summary("Good book").build();
        boolean b1 = bookRepository.create(book);
        assertTrue(b1);
        Book book1 = Book.builder().author(author).isBorrow(false).category("Thriller").isbn("A57693JHGJH").idBook(book.getIdBook())
                .releaseDate(LocalDate.of(2012, 2, 22)).title("Witcher").summary("Good book").build();
        boolean b2 = bookRepository.update(book1);
        assertTrue(b2);
    }

    @Test
    public void getBook(){
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean b = authorRepository.create(author);
        assertTrue(b);
        Book book = Book.builder().author(author).isBorrow(true).category("Horror").isbn("A57693JHGJH")
                .releaseDate(LocalDate.of(2012, 2, 22)).title("Witcher").summary("Good book").build();
        boolean b1 = bookRepository.create(book);
        assertTrue(b1);
        Book book1 = bookRepository.read(1L);
        System.out.println(book1);
    }



    @Test
    public void createBorrowerDetails_shouldReturnTrue(){
        BorrowerDetails borrowerDetails = BorrowerDetails.builder().address("Dluga 53").email("wojtas@.pl").phone("22332223").build();
        boolean b = borrowerDetailsRepository.create(borrowerDetails);
        assertTrue(b);
    }
    @Test
    public void updateBorrowerDetails_shouldReturnTrue(){
        BorrowerDetails borrowerDetails = BorrowerDetails.builder().address("Dluga 53").email("wojtas@.pl").phone("22332223").build();
        BorrowerDetails borrowerDetails1 = BorrowerDetails.builder().address("Dluga 53").email("wojtas@.pl")
                .phone("8888888").idBorrowerDetails(borrowerDetails.getIdBorrowerDetails()).build();
        boolean b = borrowerDetailsRepository.create(borrowerDetails);
        boolean b1 = borrowerDetailsRepository.update(borrowerDetails1);
        assertTrue(b);
        assertTrue(b1);
    }
    @Test
    public void createBorrower_shouldReturnTrue(){
        BorrowerDetails borrowerDetails = BorrowerDetails.builder().address("Dluga 53").email("wojtas@.pl").phone("22332223").build();
        boolean b = borrowerDetailsRepository.create(borrowerDetails);
        assertTrue(b);
        Borrower borrower = Borrower.builder().firstName("Wojtas").lastName("Kowalski").borrowerDetails(borrowerDetails).build();
        boolean b1 = borrowerRepository.create(borrower);
        assertTrue(b1);
    }
    @Test
    public void createBorrow_shouldReturnTrue(){
        BorrowerDetails borrowerDetails = BorrowerDetails.builder().address("Dluga 53").email("wojtas@.pl").phone("22332223").build();
        boolean b = borrowerDetailsRepository.create(borrowerDetails);
        assertTrue(b);
        Borrower borrower = Borrower.builder().firstName("Wojtas").lastName("Kowalski").borrowerDetails(borrowerDetails).build();
        boolean c = borrowerRepository.create(borrower);
        assertTrue(c);
        Author author = Author.builder().firstName("Jacek").lastName("Wicek").birthPlace("Radom").build();
        boolean d = authorRepository.create(author);
        assertTrue(d);
        Book book = Book.builder().author(author).isBorrow(false).category("Horror").isbn("A57693JHGJH")
                .releaseDate(LocalDate.of(2012, 2, 22)).title("Witcher").summary("Good book").build();
        boolean e = bookRepository.create(book);
        assertTrue(e);
        Borrow borrow = Borrow.builder().book(book).borrower(borrower).rentalDate
                (LocalDate.of(2011, 2, 23)).build();
        boolean z = borrowRepository.create(borrow);
        assertTrue(z);
    }

//    @After
//    public void cleanup(){
//        EntityManager em = EntityManagerRepository.INSTANCE;
//        em.getTransaction().begin();
//     //   em.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0;").executeUpdate();
//        em.createNativeQuery("delete from borrow").executeUpdate();
//        em.createNativeQuery("delete from book").executeUpdate();
//        em.createNativeQuery("delete from author").executeUpdate();
//        em.createNativeQuery("delete from borrower").executeUpdate();
//        em.createNativeQuery("delete from borrower_details").executeUpdate();
//   //     em.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1;").executeUpdate();
//        em.getTransaction().commit();
//    }

    @Test
    public void getAuthor_shouldReturnAuthor(){
        Author author = Author.builder().lastName("Marecki").firstName("Marek").birthPlace("Piaseczno").build();
        authorRepository.create(author);
        Author author1 = authorRepository.getAuthorIfExistElseCreate("aaa Marecki");
        System.out.println(author1);
    }

    @Test
    public void getBorrowerIfExistElseCreate_shouldReturnBorrower(){
        BorrowerDetails borrowerDetails = BorrowerDetails.builder().address("Dluga 53").email("wojtas@.pl").phone("22332223").build();
        boolean c = borrowerDetailsRepository.create(borrowerDetails);
        Borrower borrower = Borrower.builder().firstName("Wojciech")
                .lastName("Kowalski").borrowerDetails(borrowerDetails).build();
        boolean b = borrowerRepository.create(borrower);
        assertTrue(b);
        Borrower borrower1 = borrowerRepository.getBorrowerIfExistElseCreate
                ("Wojciech Kowalski","Sucharskiego 53", "wojtek@wp", "344444");
        assertEquals(borrower, borrower1);

    }
}
