package pl.sda.model.repository;

import pl.sda.model.Author;
import pl.sda.model.Borrow;
import pl.sda.model.Borrower;
import pl.sda.model.BorrowerDetails;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class BorrowerRepository extends GenericDao<Borrower, Long> {

    private BorrowerDetailsRepository borrowerDetailsRepository = new BorrowerDetailsRepository();

    public static final BorrowerRepository INSTANCE = new BorrowerRepository();

    public static BorrowerRepository createBorrowerRepository(){
        return INSTANCE;
    }

    public Borrower getBorrowerIfExistElseCreate(String borrowerFirstAndLastName, String address, String email, String phone) {
        List borrowers1 = new ArrayList<>();
        List borrowers2 = new ArrayList<>();
        String[] borrowerFirstnameAndLastname = borrowerFirstAndLastName.split(" ");
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Query query = entityManager.createQuery
                    ("Select a from Borrower a where a.firstName = :firstName and a.lastName = :lastName");
            query.setParameter("firstName", borrowerFirstnameAndLastname[0]);
            query.setParameter("lastName", borrowerFirstnameAndLastname[1]);
            borrowers1 = query.getResultList();
            transaction.commit();
            if (borrowers1.isEmpty()) {
                BorrowerDetails borrowerDetails = BorrowerDetails.builder()
                        .address(address).email(email).phone(phone).build();
                borrowerDetailsRepository.create(borrowerDetails);
                create(Borrower.builder().firstName(borrowerFirstnameAndLastname[0]).
                        lastName(borrowerFirstnameAndLastname[1]).borrowerDetails(borrowerDetails).build());
                try {
                    transaction.begin();
                    Query query1 = entityManager.createQuery
                            ("Select a from Borrower a where a.firstName = :firstName and a.lastName = :lastName");
                    query1.setParameter("firstName", borrowerFirstnameAndLastname[0]);
                    query1.setParameter("lastName", borrowerFirstnameAndLastname[1]);
                    borrowers2 = query1.getResultList();
                    transaction.commit();
                } catch (Exception e) {
                    transaction.rollback();
                    return null;
                }

                return (Borrower) borrowers2.get(0);
            }
            return (Borrower) borrowers1.get(0);
        } catch (Exception e) {
            transaction.rollback();
            return null;
        }

    }
}
