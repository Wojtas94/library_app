package pl.sda.model.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerRepository {
    public static EntityManagerFactory factory = Persistence.createEntityManagerFactory("LibraryAppPersistenceUnit");

    public static final EntityManager INSTANCE = factory.createEntityManager();

    public static EntityManager createEntityManagerRepository(){
        return INSTANCE;
    }
}
