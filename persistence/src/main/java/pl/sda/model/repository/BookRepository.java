package pl.sda.model.repository;

import pl.sda.model.Book;

public class BookRepository extends GenericDao<Book, Long> {

    public static final BookRepository INSTANCE = new BookRepository();

    public static BookRepository createBookRepository(){
        return INSTANCE;
    }
}
