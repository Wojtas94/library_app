package pl.sda.model.repository;

import pl.sda.model.Author;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class AuthorRepository extends GenericDao<Author, Long> {

    public static final AuthorRepository INSTANCE = new AuthorRepository();

    public static AuthorRepository createAuthorRepository() {
        return INSTANCE;
    }

    public Author getAuthorIfExistElseCreate(String authorFirstAndLastName) {
        List authors1 = new ArrayList<>();
        List authors2 = new ArrayList<>();
        String[] authorFirstnameAndLastname = authorFirstAndLastName.split(" ");
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Query query = entityManager.createQuery
                    ("Select a from Author a where a.firstName = :firstName and a.lastName = :lastName");
            query.setParameter("firstName", authorFirstnameAndLastname[0]);
            query.setParameter("lastName", authorFirstnameAndLastname[1]);
            authors1 = query.getResultList();
            transaction.commit();
            if (authors1.isEmpty()) {
                create(Author.builder().firstName(authorFirstnameAndLastname[0]).
                        lastName(authorFirstnameAndLastname[1]).build());
                try {
                    transaction.begin();
                    Query query1 = entityManager.createQuery
                            ("Select a from Author a where a.firstName = :firstName and a.lastName = :lastName");
                    query1.setParameter("firstName", authorFirstnameAndLastname[0]);
                    query1.setParameter("lastName", authorFirstnameAndLastname[1]);
                    authors2 = query1.getResultList();
                    transaction.commit();
                } catch (Exception e) {
                    transaction.rollback();
                    return null;
                }

                return (Author) authors2.get(0);
            }
            return (Author) authors1.get(0);
        } catch (Exception e) {
            transaction.rollback();
            return null;
        }

    }


}
