package pl.sda.model.repository;

import pl.sda.model.BorrowerDetails;

public class BorrowerDetailsRepository extends GenericDao<BorrowerDetails, Long> {

    public static final BorrowerDetailsRepository INSTANCE = new BorrowerDetailsRepository();

    public static BorrowerDetailsRepository createBorrowerDetailsRepository(){
        return INSTANCE;
    }
}
