package pl.sda.model.repository;

import pl.sda.model.Borrow;

public class BorrowRepository extends GenericDao<Borrow, Long> {

    public static final BorrowRepository INSTANCE = new BorrowRepository();

    public static BorrowRepository createBorrowRepository(){
        return INSTANCE;
    }
}
