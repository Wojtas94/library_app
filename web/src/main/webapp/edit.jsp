<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 29.09.2019
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Library</title>
</head>
<body>
<jsp:include page="/WEB-INF/navbar.jspf"/>
<div class = "container">
    <form action="/EditServlet" method="post">
        <div class="form-group">
            <label for="exampleFormControlInput1">Title</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title" value="${bookDto.title}">
        </div>


        <div class="form-group">
            <label for = "author">Author</label>
            <input type="text" class="form-control" id="author" name = "author" value="${bookDto.author}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">ISBN</label>
            <input type="text" class="form-control" id="exampleFormControlInput2" name="isbn" value=${bookDto.isbn}>
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Pages</label>
            <input type="number" class="form-control" id="exampleFormControlInput3" name="pages" value=${bookDto.pages}>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Category</label>
            <select class="form-control" name="category" id="exampleFormControlSelect1">
                <option>Horror</option>
                <option>Fantasy</option>
                <option>Thriller</option>
                <option>Comedy</option>
                <option>Drama</option>
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Release date</label>
            <input type="date" class="form-control" id="exampleFormControlInput4" name="date" value=${bookDto.dateOfRelease}>
        </div>

        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="HomeServlet"><button type="button" class="btn btn-danger">Cancel</button></a>
    </form>

</div>
<%@ include file="/WEB-INF/lowbar.jspf"%>
</body>
</html>