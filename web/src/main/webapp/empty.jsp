<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Library</title>
</head>
<body>
<jsp:include page="/WEB-INF/navbar.jspf"/>
<div class="container">
    <form action="/HomeServlet" method="post">
        <div class="alert alert-primary" role="alert">
            The library does not have available books.
        </div>
        <button type="submit" class="btn btn-primary" name="action" value="ADD">Add</button>
    </form>

</div>
<%@ include file="/WEB-INF/lowbar.jspf" %>
</body>
</html>