<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Info</title>
</head>
<body>
<jsp:include page="/WEB-INF/navbar.jspf"/>
<div class = "container">
    <form action="/InfoServlet" method = "post">
        <table class="table table-user-information">
            <tbody>
            <tr>
                <td>Title:</td>
                <td>${bookDto.title}</td>
            </tr>
            <tr>
                <td>Author:</td>
                <td>${bookDto.author}</td>
            </tr>
            <tr>
                <td>Category:</td>
                <td>${bookDto.category}</td>
            </tr>
            <tr>
            <tr>
                <td>ISBN:</td>
                <td>${bookDto.isbn}</td>
            </tr>
            <tr>
                <td>Date of release:</td>
                <td>${bookDto.dateOfRelease}</td>
            </tr>
            <tr>
                <td>Pages:</td>
                <td>${bookDto.pages}</td>
            </tr>
            <tr>
                <td>Borrower:</td>
                <td>${not empty bookDto.borrower ? bookDto.borrower : '-'}</td>
            </tr>
            </tr>

            </tbody>
        </table>
    <a href="HomeServlet"><button type="button" class="btn btn-secondary">Start Panel</button></a>

    </form>
</div>
<%@ include file="/WEB-INF/lowbar.jspf"%>
</body>
</html>