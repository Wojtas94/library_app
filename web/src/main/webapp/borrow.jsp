<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Borrow</title>
</head>
<body>
<jsp:include page="/WEB-INF/navbar.jspf"/>
<div class="container">
    <form action="/BorrowServlet" method="post">
        <div class="alert alert-warning" role="alert">
            Are you sure you want to borrow book <strong>${bookDto.title}</strong>
        </div>
        <br>
        <div class="form-group">
            <label for="Name">Your name</label>
            <input type="text" class="form-control" id="Name" name="name" placeholder="Name">
        </div>
        <div class="form-group">
            <label for="Surname">Your surname</label>
            <input type="text" class="form-control" id="Surname" name="surname" placeholder="Surname">
        </div>
        <div class="form-group">
            <label for="Email">Your email</label>
            <input type="text" class="form-control" id="Email" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="Phone">Your Phone Number</label>
            <input type="text" class="form-control" id="Phone" name="phone" placeholder="Phone">
        </div>
        <div class="form-group">
            <label for="Phone">Your Address</label>
            <input type="text" class="form-control" id="Address" name="address" placeholder="Address">
        </div>
        <button type="submit" class="btn btn-success">Borrow</button>
        <a href="/HomeServlet"><button type="button" class="btn btn-danger">Cancel</button></a>
    </form>
</div>

<%@ include file="/WEB-INF/lowbar.jspf" %>
</body>
</html>
