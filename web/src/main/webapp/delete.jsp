<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Library</title>
</head>
<body>
<jsp:include page="/WEB-INF/navbar.jspf"/>
<div class = "container">
    <form action="/DeleteServlet" method="post">
        <div class="alert alert-danger" role="alert">
            Are you sure you want to delete book <strong>${book.title}</strong>
        </div>
        <button type="submit" class="btn btn-success">Delete</button>
        <a href="/HomeServlet"><button type="button" class="btn btn-danger">Cancel</button></a>
    </form>
</div>
<%@ include file="/WEB-INF/lowbar.jspf"%>
</body>
</html>