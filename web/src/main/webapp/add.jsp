<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Library</title>
</head>
<body>
<jsp:include page="/WEB-INF/navbar.jspf"/>
<div class = "container">
    <form action="/AddServlet" method="post">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="form-group">
            <label for = "author">Author</label>
            <input type="text" class="form-control" id="author" name = "author" >
        </div>
        <div class="form-group">
            <label for="isbn">ISBN</label>
            <input type="text" class="form-control" id="isbn" name = "isbn">
        </div>
        <div class="form-group">
            <label for="pages">Pages</label>
            <input type="number" class="form-control" id="pages" name = "pages">
        </div>
        <div class="form-group">
            <label for="category">Category</label>
            <select class="form-control" id="category" name="category">
                <option>Horror</option>
                <option>Fantasy</option>
                <option>Thriller</option>
                <option>Comedy</option>
                <option>Drama</option>
            </select>
        </div>
        <div class="form-group">
            <label for="date">Release date</label>
            <input type="date" class="form-control" id="date" name = "date">
        </div>
        <button type="submit" class="btn btn-success">Add</button>
        <a href="/HomeServlet"><button type="button" class="btn btn-danger">Cancel</button></a>
    </form>

</div>
<%@ include file="/WEB-INF/lowbar.jspf"%>
</body>
</html>