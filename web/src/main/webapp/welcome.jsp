<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Library</title>
</head>
<body>
<jsp:include page="/WEB-INF/navbar.jspf"/>
<div class="container">
    <form action="/HomeServlet" method="post">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Title</th>
                <th scope="col">ISBN</th>
                <th scope="col">Author</th>
                <th scope="col">Type</th>
                <th scope="col">Release</th>
                <th scope="col">Pages</th>
                <th scope="col">Borrower</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="book" items="${requestScope.books}">
                <tr>

                    <td>${book.bookId}</td>
                    <td>${book.title}</td>
                    <td>${book.isbn}</td>
                    <td>${book.author}</td>
                    <td>${book.category}</td>
                    <td>${book.dateOfRelease}</td>
                    <td>${book.pages}</td>
                    <td>${not empty book.borrower ? book.borrower : '-'}</td>
                    <td><input class="form-checked-input" type="radio" name="bookId" checked value="${book.bookId}"></td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
        <button type="submit" class="btn btn-primary" name="action" value="ADD">Add</button>
        <button type="submit" class="btn btn-secondary" name="action" value="INFO">Info</button>
        <button type="submit" class="btn btn-success" name="action" value="EDIT">Edit</button>
        <button type="submit" class="btn btn-danger" name="action" value="DELETE">Delete</button>
        <button type="submit" class="btn btn-warning" name="action" value="BORROW">Borrow</button>
    </form>

</div>
<%@ include file="/WEB-INF/lowbar.jspf" %>
</body>
</html>
