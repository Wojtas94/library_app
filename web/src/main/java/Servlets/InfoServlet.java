package Servlets;

import Services.BookServices;
import pl.sda.dto.BookDto;
import pl.sda.model.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/InfoServlet")
public class InfoServlet extends HttpServlet {

    BookServices bookServices = new BookServices();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("bookId"));
        BookDto bookDto = bookServices.getBookDtoByBookId(id);
        req.setAttribute("bookDto", bookDto);
        HttpSession session = req.getSession();
        session.setAttribute("id", id);
        req.getRequestDispatcher("info.jsp").forward(req, resp);;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/HomeServlet");
    }
}
