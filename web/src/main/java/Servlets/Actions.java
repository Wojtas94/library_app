package Servlets;

public enum Actions {
    ADD,
    EDIT,
    INFO,
    BORROW,
    DELETE;
}
