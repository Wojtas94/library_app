package Servlets;

import Services.BookServices;
import pl.sda.dto.BookDto;
import pl.sda.model.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {

    BookServices bookServices = new BookServices();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        bookServices.removeBook((Long) req.getSession().getAttribute("id"));
        resp.sendRedirect("/HomeServlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long idBook = Long.valueOf(req.getParameter("bookId"));
            Book book = bookServices.getBookFromGivenId(idBook);
            HttpSession session = req.getSession();
            session.setAttribute("id", idBook);
            req.setAttribute("book", book);
            req.getRequestDispatcher("delete.jsp").forward(req, resp);
    }
}
