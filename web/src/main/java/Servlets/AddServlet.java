package Servlets;

import Services.BookServices;
import pl.sda.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/AddServlet")
public class AddServlet extends HttpServlet {

    private BookServices bookServices = new BookServices();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String isbn = req.getParameter("isbn");
        Integer pages = Integer.valueOf(req.getParameter("pages"));
        String category = req.getParameter("category");
        LocalDate date = LocalDate.parse(req.getParameter("date"));
        String author = req.getParameter("author");
        bookServices.createBookFromBookDto(BookDto.builder().title(title).author(author).isbn(isbn).pages(pages).
                category(category).dateOfRelease(date).isBorrow(false).build());
        resp.sendRedirect("/HomeServlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       req.getRequestDispatcher("add.jsp").forward(req, resp);
    }
}
