package Servlets;

import Services.BookServices;
import pl.sda.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {

    private BookServices bookServices = new BookServices();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Actions actions = Actions.valueOf(req.getParameter("action"));
        Long bookId = null;
        try{
            bookId = Long.valueOf(req.getParameter("bookId"));
        }catch (NumberFormatException e){

        }
        switch (actions){
            case ADD:
                resp.sendRedirect("/AddServlet");
                break;
            case EDIT:
                resp.sendRedirect("/EditServlet?bookId=" + bookId);
                break;
            case DELETE:
                resp.sendRedirect("/DeleteServlet?bookId=" + bookId);
                break;
            case BORROW:
                resp.sendRedirect("/BorrowServlet?bookId=" + bookId);
                break;
            case INFO:
                resp.sendRedirect("/InfoServlet?bookId=" + bookId);
                break;
            default: throw new IllegalArgumentException("bad argument");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<BookDto> books = bookServices.findAllDtoBooks();
        if(books.isEmpty()){
            req.getRequestDispatcher("empty.jsp").forward(req, resp);

        }
        req.setAttribute("books", books);
        req.getRequestDispatcher("welcome.jsp").forward(req, resp);
    }

}
