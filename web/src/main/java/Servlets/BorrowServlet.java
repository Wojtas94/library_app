package Servlets;

import Services.BookServices;
import Services.BorrowService;
import Services.BorrowerService;
import pl.sda.dto.BookDto;
import pl.sda.model.Borrow;
import pl.sda.model.Borrower;
import pl.sda.model.repository.BorrowRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/BorrowServlet")
public class BorrowServlet extends HttpServlet {

    BookServices bookServices = new BookServices();
    BorrowService borrowService = new BorrowService();
    BorrowerService borrowerService = new BorrowerService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long bookId = Long.valueOf(req.getParameter("bookId"));
        BookDto bookDto = bookServices.getBookDtoByBookId(bookId);
        req.getSession().setAttribute("bookDto", bookDto);
        req.getSession().setAttribute("bookId", bookId);
        req.getRequestDispatcher("borrow.jsp").forward(req, resp);;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String borrowerName = req.getParameter("name");
        String borrowerSurname = req.getParameter("surname");
        String borrowerPhone = req.getParameter("phone");
        String borrowerEmail = req.getParameter("email");
        String borrowerAddress = req.getParameter("address");
        Long id = (Long) req.getSession().getAttribute("bookId");
        BookDto bookBeforeUpdate = bookServices.getBookDtoByBookId(id);
        List<Borrow> borrows = new ArrayList<>();
        Borrow borrow = borrowService.createBorrow(Borrow.builder().book(bookServices.getBookFromGivenId(
                (Long) req.getSession().getAttribute("bookId"))).rentalDate(LocalDate.now())
                .borrower(borrowerService.getBorrowerIfExistElseCreate(borrowerName + " " + borrowerSurname
                        , borrowerAddress, borrowerEmail, borrowerPhone)).build());
        borrows.add(borrow);
        BookDto bookAfterUpdate = BookDto.builder().bookId(bookBeforeUpdate.getBookId())
                .author(bookBeforeUpdate.getAuthor()).borrower(borrowerName + " " + borrowerSurname)
                .category(bookBeforeUpdate.getCategory()).dateOfRelease(bookBeforeUpdate.getDateOfRelease())
                .isBorrow(true).isbn(bookBeforeUpdate.getIsbn()).pages(bookBeforeUpdate.getPages())
                .title(bookBeforeUpdate.getTitle()).borrows(borrows).build();
        bookServices.updateBookfromBookDto(bookAfterUpdate);
        resp.sendRedirect("/HomeServlet");
    }
}
