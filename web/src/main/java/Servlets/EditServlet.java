package Servlets;

import Services.BookServices;
import pl.sda.dto.BookDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {

    BookServices bookServices = new BookServices();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("bookId"));
        BookDto bookDto = bookServices.getBookDtoByBookId(id);
        req.setAttribute("bookDto", bookDto);
        req.getSession().setAttribute("isBorrow", bookDto.getIsBorrow());
        req.getSession().setAttribute("borrower", bookDto.getBorrower());
        HttpSession session = req.getSession();
        session.setAttribute("id", id);
        req.getRequestDispatcher("edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String isbn = req.getParameter("isbn");
        Integer pages = Integer.valueOf(req.getParameter("pages"));
        String category = req.getParameter("category");
        LocalDate date = LocalDate.parse(req.getParameter("date"));
        String author = req.getParameter("author");
        Long id = (Long) req.getSession().getAttribute("id");
        bookServices.updateBookfromBookDto(BookDto.builder().author(author).category(category).dateOfRelease(date)
                .isbn(isbn).pages(pages).title(title).bookId(id)
                .borrower((String) req.getSession().getAttribute("borrower"))
                .isBorrow((Boolean) req.getSession().getAttribute("isBorrow")).build());
        resp.sendRedirect("/HomeServlet");
    }
}
