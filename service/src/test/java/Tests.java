import Services.BookServices;
import org.junit.Before;
import org.junit.Test;
import pl.sda.dto.BookDto;

import java.time.LocalDate;

public class Tests {

    BookServices bookServices;

    @Before
    public void start(){
        bookServices = new BookServices();
    }

    @Test
    public void createTest(){
        bookServices.createBookFromBookDto(BookDto.builder().pages(333).category("Horror").
                author("Marek Marecki").title("W pustyni").
                dateOfRelease(LocalDate.of(2000, 3, 22)).build());

    }
}
