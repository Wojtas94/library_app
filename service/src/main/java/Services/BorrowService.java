package Services;

import pl.sda.model.Borrow;
import pl.sda.model.repository.BorrowRepository;

public class BorrowService {

    BorrowRepository borrowRepository = new BorrowRepository();

    public Borrow createBorrow(Borrow borrow){
        borrowRepository.create(borrow);
        return borrow;
    }
}
