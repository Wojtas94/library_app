package Services;

import pl.sda.model.Borrower;
import pl.sda.model.repository.BorrowerRepository;

public class BorrowerService {

    private BorrowerRepository borrowerRepository = new BorrowerRepository();

    public Borrower getBorrowerIfExistElseCreate(String borrowerFirstAndLastName, String address, String email, String phone){
       return borrowerRepository.getBorrowerIfExistElseCreate(borrowerFirstAndLastName, address, email, phone);
    }


}
