package Services;

import pl.sda.model.BorrowerDetails;
import pl.sda.model.repository.BorrowerDetailsRepository;

public class BorrowerDetailsService {

    private BorrowerDetailsRepository borrowerDetailsRepository = new BorrowerDetailsRepository();

    public void createBorrowerDetails(BorrowerDetails borrowerDetails){
        borrowerDetailsRepository.create(borrowerDetails);
    }
}
