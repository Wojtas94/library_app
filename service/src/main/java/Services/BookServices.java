package Services;

import pl.sda.dto.BookDto;
import pl.sda.model.Book;
import pl.sda.model.Borrow;
import pl.sda.model.Borrower;
import pl.sda.model.repository.AuthorRepository;
import pl.sda.model.repository.BookRepository;
import pl.sda.model.repository.BorrowerRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BookServices {

    private BookRepository bookRepository = new BookRepository();
    private BorrowerRepository borrowerRepository = new BorrowerRepository();
    private AuthorRepository authorRepository = new AuthorRepository();

    private String findBorrower(Book book){
        String borrowerName = "";
        if (book.getIsBorrow()){
            Optional<Borrower> borrower = book.getBorrows().stream()
                    .max(Comparator.comparing(Borrow::getIdBorrow))
                    .map(Borrow::getBorrower);
            if (borrower.isPresent()) {
                borrowerName = borrower.get().getBorrowerName();
            }
        }
        return borrowerName;
    }



    public BookDto getBookDtoByBookId(Long bookId){
        Book b = bookRepository.read(bookId);
        String borrowerName = findBorrower(b);
        String author = b.getAuthor().getAuthorName();
        System.out.println(author);
        return new BookDto(b.getIdBook(), b.getTitle(), b.getIsbn(),
                b.getAuthor().getAuthorName(),b.getCategory(), b.getReleaseDate(), b.getPages(), borrowerName, b.getIsBorrow(), b.getBorrows());
    }

    public List<BookDto> findAllDtoBooks() {
        List<Book> books = bookRepository.findAll();

        return books.stream().map(b ->  {
           String borrowerName = findBorrower(b);
           return new BookDto(b.getIdBook(), b.getTitle(), b.getIsbn(),
                b.getAuthor().getAuthorName(),b.getCategory(), b.getReleaseDate(), b.getPages(), borrowerName, b.getIsBorrow(), b.getBorrows());
        }).collect(Collectors.toList());
    }

    public List<BookDto> findAllAvailableBooks() {
        List<BookDto> books = findAllDtoBooks();
        return books.stream()
                .filter(b -> b.getIsBorrow().equals(false))
                .collect(Collectors.toList());
    }

    public Book getBookFromGivenId(Long id){
       return bookRepository.read(id);
    }

    public Book createBookFromBookDto(BookDto bookDto) {
        Book book = Book.builder().author(authorRepository.getAuthorIfExistElseCreate(bookDto.getAuthor())).
                category(bookDto.getCategory().toUpperCase()).isbn(bookDto.getIsbn()).pages(bookDto.getPages()).
                releaseDate(bookDto.getDateOfRelease()).isBorrow(bookDto.getIsBorrow()).title(bookDto.getTitle()).
                borrows(bookDto.getBorrows()).build();
        bookRepository.create(book);
        return book;
    }

    public void removeBook(Long id){
        bookRepository.delete(id);
    }

    public void updateBookfromBookDto(BookDto bookDto){
        Book book = Book.builder().author(authorRepository.getAuthorIfExistElseCreate(bookDto.getAuthor())).
                category(bookDto.getCategory().toUpperCase()).isbn(bookDto.getIsbn()).pages(bookDto.getPages()).
                releaseDate(bookDto.getDateOfRelease()).title(bookDto.getTitle()).idBook(bookDto.getBookId()).
                isBorrow(bookDto.getIsBorrow()).borrows(bookDto.getBorrows()).build();
        bookRepository.update(book);
    }

    public void borrowBook(BookDto bookAfterUpdate) {
        Book bookFromBookDto = createBookFromBookDto(bookAfterUpdate);
        bookRepository.update(bookFromBookDto);
    }
}
